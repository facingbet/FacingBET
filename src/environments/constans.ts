export const constants = {
  routes: {
    home: {
      name: 'inicio',
      path: '/inicio'
    },
    sports: {
      name: 'deportes',
      path: '/deportes'
    },
    live: {
      name: 'en-vivo',
      path: '/en-vivo'
    },
    gaming: {
      name: 'juegos-de-azar',
      path: '/guegos-de-azar'
    },
    promotions: {
      name: 'promocionesr',
      path: '/promociones'
    },
  },
  buttons: {
    colors: {
      primary: 'primary',
      success: 'success',
      light: 'light',
      dark: 'dark'
    },
    sizes: {
      small: 25,
      medium: 36,
      large: 50
    }
  }
};

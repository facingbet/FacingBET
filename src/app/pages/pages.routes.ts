import { Routes } from '@angular/router';

// environments
import { constants } from '@environments/constans';

// pages
import { HomeComponent } from './home/home.component';
import { SportsComponent } from './sports/sports.component';
import { LiveComponent } from './live/live.component';
import { GamingComponent } from './gaming/gaming.component';
import { PromotionsComponent } from './promotions/promotions.component';

export const pagesRoutes: Routes = [
  {
    path: constants.routes.home.name,
    component: HomeComponent
  },
  {
    path: constants.routes.sports.name,
    component: SportsComponent
  },
  {
    path: constants.routes.live.name,
    component: LiveComponent,
  },
  {
    path: constants.routes.gaming.name,
    component: GamingComponent
  },
  {
    path: constants.routes.promotions.name,
    component: PromotionsComponent
  },
  {
    path: '',
    redirectTo: constants.routes.home.path,
    pathMatch: 'full'
  },
  {
    path: '**',
    component: HomeComponent
  }
];

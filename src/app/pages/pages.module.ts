import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routes
import { pagesRoutes } from './pages.routes';

// Pages
import { HomeComponent } from './home/home.component';
import { SportsComponent } from './sports/sports.component';
import { GamingComponent } from './gaming/gaming.component';
import { LiveComponent } from './live/live.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    HomeComponent,
    SportsComponent,
    LiveComponent,
    GamingComponent,
    PromotionsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(pagesRoutes)
  ],
  exports: [
    // Pages
    HomeComponent,
    SportsComponent,
    LiveComponent,
    GamingComponent,
    PromotionsComponent,

    // Routes
    RouterModule
  ]
})
export class PagesModule { }

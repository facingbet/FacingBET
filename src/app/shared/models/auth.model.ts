export default class Auth {

  /**
   * codigo de registro del usuario dentro de la app
   */
  public uid: string;

  /**
   * numero de la cedula de identidad del usuario
   */
  public run: string;

  /**
   * nombre del usuario
   */
  public name: string;

  /**
   * apellido del usuario
   */
  public lastname: string;

  /**
   * nombre del usuario dentro de la app
   */
  public username: string;

  /**
   * si el usuario tiene el correo verificado o no
   */
  public verifyEmail: boolean;

  constructor() {}
}

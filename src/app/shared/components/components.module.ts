import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './header/navbar/navbar.component';
import { BtnHoverLeftComponent } from './buttons/btn-hover-left/btn-hover-left.component';

@NgModule({
  declarations: [
    HeaderComponent,
    NavbarComponent,
    BtnHoverLeftComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    HeaderComponent,
    BtnHoverLeftComponent
  ]
})
export class ComponentsModule { }

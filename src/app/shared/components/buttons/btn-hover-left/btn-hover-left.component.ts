import { Component, OnInit, Input } from '@angular/core';

// environments
import { constants } from '@environments/constans';

@Component({
  selector: 'app-btn-hover-left',
  templateUrl: './btn-hover-left.component.html',
  styleUrls: ['./btn-hover-left.component.scss']
})
export class BtnHoverLeftComponent implements OnInit {

  /**
   * tamano del boton
   *
   * [small, medium, large]
   */
  @Input() public size = constants.buttons.sizes.medium;

  /**
   * titulo del boton
   */
  @Input() public title: string;

  /**
   * color del boton
   *
   * [primary, success, danger, light, dark]
   */
  @Input() public color = constants.buttons.colors.primary;

  /**
   * icono a mostrar
   */
  @Input() public icon: string;

  /**
   * referencia a las constantes
   */
  public constants: any = constants;

  constructor() { }

  ngOnInit() {}
}

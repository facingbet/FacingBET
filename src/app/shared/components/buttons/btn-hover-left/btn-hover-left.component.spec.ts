import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnHoverLeftComponent } from './btn-hover-left.component';

describe('BtnHoverLeftComponent', () => {
  let component: BtnHoverLeftComponent;
  let fixture: ComponentFixture<BtnHoverLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnHoverLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnHoverLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

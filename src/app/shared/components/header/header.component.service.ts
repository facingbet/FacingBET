import { Injectable } from '@angular/core';

// Services
import { HeaderService } from '@services/index.services';

@Injectable({
  providedIn: 'root',
})
export class HeaderComponentService {

  constructor(public headerService: HeaderService) { }

  /**
   * nos devuelve el nombre del usuario
   */
  public get username(): string {
    return this.headerService.username;
  }

  /**
   * nos dice si el correo fue verificado o no
   */
  public get isEmailVerify(): boolean {
    return this.headerService.isEmailVerify;
  }

  /**
   * nos dice si el usuario esta loggeado
   */
  public get isLogged(): boolean {
    return this.headerService.isLogged;
  }

  /**
   * retorna el icono que se mostrara para
   * validar si el usuario fue validado
   */
  public get iconVerifyEmail(): string {
    return this.isEmailVerify
      ? 'far fa-check-circle'
      : 'far fa-times-circle';
  }
}

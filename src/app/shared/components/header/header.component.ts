import { Component, OnInit } from '@angular/core';

// services
import { HeaderComponentService } from './header.component.service';

// environments
import { constants } from '@environments/constans';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [HeaderComponentService]
})
export class HeaderComponent implements OnInit {

  /**
   * Referencia a las constantes
   */
  public constants: any = constants;

  constructor(public headerComponentService: HeaderComponentService) {}

  ngOnInit() {
  }

  /**
   * obtenemos el servicio del componente
   */
  public get _header(): HeaderComponentService {
    return this.headerComponentService;
  }

}

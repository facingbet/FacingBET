import { NgModule } from '@angular/core';

// services
import {
  AuthService,
  HeaderService
} from './index.services';

@NgModule({
  providers: [
    AuthService,
    HeaderService
  ]
})
export class ServicesModule { }

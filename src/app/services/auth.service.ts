import { Injectable } from '@angular/core';

// Models
import Auth from '@models/auth.model';


@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private isLogin: boolean;
  private auth: Auth;

  constructor() {
    // TODO: borrar luego solo datos de prueba
    this.auth = new Auth();
    this.isLogin = false;
    this.auth.verifyEmail = true;
    this.auth.name = 'Matias';
    this.auth.lastname = 'Espinoza';
    this.auth.username = 'mespinoza';
    this.auth.run = '185387054';
    this.auth.uid = '123456';
  }

  /**
   * nos dice si el usuario esta loggeado o no
   */
  public isAuthLogged(): boolean {
    return this.isLogin || false;
  }

  /**
   * nos devuelve el usuario autenticado
   */
  public getAuth(): Auth {
    return this.auth || null;
  }
}

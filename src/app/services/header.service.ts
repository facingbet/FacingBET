import { Injectable } from '@angular/core';

// Services
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {


  constructor(private authService: AuthService) {}

  /**
   * nos devuelve el nombre del usuario
   */
  public get username(): string {
    return this.authService.getAuth().username;
  }

  /**
   * nos dice si el correo fue verificado o no
   */
  public get isEmailVerify(): boolean {
    return this.authService.getAuth().verifyEmail;
  }

  /**
   * nos dice si el usuario esta loggeado
   */
  public get isLogged(): boolean {
    return this.authService.isAuthLogged();
  }
}
